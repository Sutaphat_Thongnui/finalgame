class Eye extends Phaser.Physics.Arcade.Sprite {
    
    constructor (scene, x, y, target) {
        super(scene, x, y, "eye");
        scene.add.existing(this);
        scene.physics.add.existing(this);

        this.target = target;
        this.radius = 180;

            scene.events.on(Phaser.Scenes.Events.UPDATE, this.processMovement, this);

        scene.anims.create({
            key : "eye_run",
            frames : scene.anims.generateFrameNumbers("eye", {start : 3, end : 11 }),
            frameRate : 10,
            repeat : -1,
        });

    }

    processMovement() {
        if (this.body == null)
            return;  

        if (Phaser.Math.Distance.BetweenPoints (
                { x : this.x ,          y : this.y },
                { x : this.target.x,    y : this.target.y }
            ) < this.radius ) 
                {
                
                    this.body.setVelocityX(this.target.x - this.x);
                    this.body.setVelocityY(this.target.y - this.y);
                    this.play("eye_run", true)
                }
        else {
                this.body.setVelocity(0);
                this.stop();
            }
    }

}