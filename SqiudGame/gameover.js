class GameOver extends Phaser.Scene {
    
    constructor() {
        super("GameOver");
    }

    init (data) {
        this.displayMessage = data.message;
    }

    create() {

        this.gameoverHUD = this.add.text(80, 250, this.displayMessage,
                                        { fontSize : '45px' , fill : '#FF0000' , fontStyle : 'bold' } );

        this.playagainHUB = this.add.text(200, 350, 'CLICK HERE TO PLAY AGAIN' ,
                                            { fontSize : '50px' , fill : '#3366FF' , fontStyle : 'bold' });

        this.playagainHUB.setInteractive();

        this.playagainHUB.on('pointerover' , () => {
            this.playagainHUB.setStyle({fill : "#FFDAB9" });
        });

        this.playagainHUB.on('pointerout' , () => {
            this.playagainHUB.setStyle({fill : "#3366FF" });
        });

        this.playagainHUB.on('pointerdown' , () => {
            this.scene.start('Sqiud');
        });
   }
}