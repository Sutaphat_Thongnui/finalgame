class GameStart extends Phaser.Scene {
    
    constructor() {
        super("GameStart");
    }

    create() {
        
       // this.gameconiHUB = this.add.text(120, 250, 'Game Conditions : collect all the keys and find a way out',
         //                                   { fontSize : '30px' , fill : '#3366FF' , fontStyle : 'bold' });

       // this.commentHUD = this.add.text(450,300,this.displayMessage,
        //                                    {fontSize : '60px',fill :'#778899'} );
                           
        this.commentagianHUD = this.add.text(150,50,'Instructions for Playing ' ,
                                            {fontSize : '20px', fill : '#FF0000' , fontStyle : 'bold' } );
            
        this.commentagianHUD = this.add.text(150,100,'First Step : Find the key to unlock the next level.' ,
            
                                            {fontSize : '20px', fill : '#3366FF' , fontStyle : 'bold' } );  
            
        this.commentagianHUD = this.add.text(150,150,'Two Step : Notice the HP at the top left.' ,
            
                                            {fontSize : '20px', fill : '#3366FF' , fontStyle : 'bold' } );       
        
        this.commentagianHUD = this.add.text(150,200,'Three Step : Watch out for monsters attacking you..' ,
            
                                            {fontSize : '20px', fill : '#3366FF' , fontStyle : 'bold' } );         
                                                   
        this.commentagianHUD = this.add.text(150,250,'Four Step : After collecting all the keys, notice the cat will open the exit to enter' ,
            
                                            {fontSize : '20px', fill : '#3366FF' , fontStyle : 'bold' } );    
                                                
        this.commentagianHUD = this.add.text(150,300,'Five Step :Find a way out. There is only one hole that can come out',
            
                                            {fontSize : '20px', fill : '#3366FF' , fontStyle : 'bold' } );    
            
        this.commentagianHUD = this.add.text(150,350,'Tip milian : Notice the good path',
            
                                            {fontSize : '20px', fill : '#3366FF' , fontStyle : 'bold' } );   
            
        this.commentagianHUD = this.add.text(150,400,'Tip Glass : Collect sugar and find a way out without a gatekeepe',
                                            {fontSize : '20px', fill : '#3366FF' , fontStyle : 'bold' } );   

        this.playgameHUB = this.add.text(150, 500, 'CLICK HERE TO GAME START' ,
                                            { fontSize : '50px' , fill : '#FF0000' , fontStyle : 'bold' });

        this.playgameHUB.setInteractive();

        this.playgameHUB.on('pointerover' , () => {
            this.playgameHUB.setStyle({fill : "#FFDAB9" });
        });

        this.playgameHUB.on('pointerout' , () => {
            this.playgameHUB.setStyle({fill : "#FF0000" });
        });

        this.playgameHUB.on('pointerdown' , () => {
            this.scene.start('Sqiud');
        });
   }
}