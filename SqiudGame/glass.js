class Glass extends Phaser.Scene {
    
    constructor() {
        super("Glass");
    }
    
    preload() {
        
        this.load.spritesheet('player', 'assets/playern.png',
                            { frameWidth: 64 , frameHeight: 64, startFrame: 0, endFrame: 35 } );

        this.load.image('terrain_image', 'assets/terrain_atlas.png'); 

        this.load.tilemapTiledJSON('map1', 'glass.json');

        this.load.audio('squidgame', 'assets/squidgame.mp3');
        
    }

    init (data) {
        this.hpCount = data.hpCount;
        this.hpDisplay = data.hpDisplay;
    }

    create() { 

        this.anims.create({
            key : 'down_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 18, end: 26 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'left_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 9, end: 17 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'right_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 27, end: 35 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'up_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 0, end: 8 }),
            frameRate : 10, repeat: -1,
        });

        this.deltaNoDamageTime = 0;

        this.map1 = this.make.tilemap({key : 'map1'});
        this.tileset = this.map1.addTilesetImage('Terrain','terrain_image');

        this.floorLayer = this.map1.createLayer('Floor',this.tileset);
        this.worldLayer = this.map1.createLayer('World',this.tileset);
        this.worldLayer.setCollisionByProperty({collision : true});

        this.sound_squidgame = this.sound.add('squidgame');
        this.sound_squidgame.play({loop:true});

        this.worldLayer.setTileLocationCallback (36 , 45 , 6 , 2,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (13 , 12 , 2 , 2,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (9 , 12 , 1 , 3,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (9 , 17 , 3 , 2,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (13 , 23 , 2 , 4,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (12 , 30 , 3 , 3,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (18 , 33 , 4 , 3,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (25 , 30 , 6 , 3,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (34 , 33 , 3 , 3,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (40 , 30 , 2 , 10,
            () => {
                
                this.scene.start("Glass" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        const spownpoint = this.map1.findObject("ObjectLayer" , obj => obj.name == "spown");
        this.player = this.physics.add.sprite( spownpoint.x , spownpoint.y , 'player');

        this.cameras.main.setBounds(0, 0, 1600, 1600);
        this.cameras.main.startFollow(this.player);
        this.cameras.main.roundPixels = true;

        this.physics.add.collider(this.player, this.worldLayer);

        this.physics.add.overlap(this.player, this.monsterGroup, (b1 ,b2) => {
            if (this.deltaNoDamageTime > 1000) {
                this.hpCount--;
                this.hpDisplay = "";
                for (let i = 0; i < this.hpCount; i++ ) {
                    this.hpDisplay += "* ";
                }
                this.hp_HUD.setText("HP : " +this.hpDisplay);
                this.deltaNoDamageTime = 0;
                

                if (this.hpCount == 0) {
                    this.scene.start("GameOver" , { message : "You died , go to hell." } );
                }


            }
        });

        this.hp_HUD = this.add.text(16, 16, "HP : " +this.hpDisplay, {fontSize : '32px' , fill : '#000000#' , fontStyle : 'bold'} );
        this.hp_HUD.setScrollFactor(0);
    }

    processInput(cursors){
                var speed_constant = 300;
                var x_speed_vector = 0;
                var y_speed_vector = 0;

           this.player.body.setVelocity(0);     

            if (cursors.up.isDown) { 
                y_speed_vector -= 1; this.player.anims.play('up_anim', true);
            }
            if (cursors.down.isDown) { 
                y_speed_vector += 1;  this.player.anims.play('down_anim', true);
            }
            if (cursors.left.isDown) { 
                x_speed_vector -= 1; this.player.anims.play('left_anim', true);
            }

            if (cursors.right.isDown) {
                x_speed_vector += 1; this.player.anims.play('right_anim', true);
            }


                if (x_speed_vector != 0 || y_speed_vector != 0) {
                    this.player.body.setVelocityX( x_speed_vector * speed_constant);
                    this.player.body.setVelocityY( y_speed_vector * speed_constant);
                    
                };
            }


           update(time, delta) {
                let cursors = this.input.keyboard.createCursorKeys();
                this.processInput(cursors);
                this.deltaNoDamageTime += delta;

            }
        }
