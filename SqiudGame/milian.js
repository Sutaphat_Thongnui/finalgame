class Milian extends Phaser.Scene {

    constructor() {
        super("Milian");
    }

    preload() {
        
        this.load.spritesheet('player', 'assets/playern.png',
                            { frameWidth: 64 , frameHeight: 64, startFrame: 0, endFrame: 35 } );

        this.load.spritesheet('eye', 'assets/eye.png',
                            { frameWidth: 32 , frameHeight: 38 } );

        this.load.image('terrain_image', 'assets/terrain_atlas.png');    

        this.load.tilemapTiledJSON('map2', 'Milian.json'); 

        this.load.audio('money', 'assets/money.mp3');

        this.load.image('sugar1', 'assets/sugar1.png');
        this.load.image('sugar2', 'assets/sugar2.png');
        this.load.image('sugar3', 'assets/sugar3.png');

    }

    init (data) {
        this.hpCount = data.hpCount;
        this.hpDisplay = data.hpDisplay;
    }

    create() { 

        this.anims.create({
            key : 'down_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 18, end: 26 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'left_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 9, end: 17 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'right_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 27, end: 35 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'up_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 0, end: 8 }),
            frameRate : 10, repeat: -1,
        });

        this.sugarCount = 3;

        this.deltaNoDamageTime = 0;

        this.map2 = this.make.tilemap({key : 'map2'});
        this.tileset = this.map2.addTilesetImage('Terrain','terrain_image');

        this.floorLayer = this.map2.createLayer('Floor',this.tileset);
        this.worldLayer = this.map2.createLayer('World',this.tileset);
        this.worldLayer.setCollisionByProperty({collision : true});

        this.sound_money = this.sound.add('money');
        this.sound_money.play({loop:true});

        this.worldLayer.setTileLocationCallback (11 , 2 , 2 , 2 ,
            () => {

                this.scene.start("Elephant" , { hpCount : this.hpCount ,
                                                hpDisplay : this.hpDisplay });

                this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (7 , 17 , 3 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (7 , 30 , 3 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (4 , 32 , 3 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (1 , 34 , 3 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (17 , 40 , 16 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (23 , 43 , 4 , 1,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (23 , 48 , 4 , 1,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (40 , 29 , 3 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (40 , 17 , 3 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (24 , 2 , 2 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (37 , 2 , 2 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        this.worldLayer.setTileLocationCallback (40 , 29 , 3 , 3,
            () => {
                
                this.scene.start("Milian" , { hpCount : this.hpCount ,
                                             hpDisplay : this.hpDisplay });

                                             this.sound.stopAll();
            }
        )

        const spownpoint = this.map2.findObject("ObjectLayer" , obj => obj.name == "spown");
        this.player = this.physics.add.sprite( spownpoint.x , spownpoint.y , 'player');

        this.cameras.main.setBounds(0, 0, 1600, 1600);
        this.cameras.main.startFollow(this.player);
        this.cameras.main.roundPixels = true;

        const sugar1 = this.map2.findObject("ObjectLayer" , obj => obj.name == "sugar1");
        const sugar2 = this.map2.findObject("ObjectLayer" , obj => obj.name == "sugar2");
        const sugar3 = this.map2.findObject("ObjectLayer" , obj => obj.name == "sugar3");

        this.sugarGroup = this.physics.add.staticGroup();
        this.sugarGroup.create( sugar1.x , sugar1.y , 'sugar1' );
        this.sugarGroup.create( sugar2.x , sugar2.y , 'sugar2' );
        this.sugarGroup.create( sugar3.x , sugar3.y , 'sugar3' );

        this.physics.add.overlap(this.player, this.sugarGroup , (b1 , b2) => {
            this.sugarCount--;
            if (this.sugarCount == 0) {
                console.log("All sugars are collected");
                this.map2.fill(34, 20 , 14 , 10 , 1);
            }
            b2.destroy();
        })

        const monster1 = this.map2.findObject("MonsterLayer" , obj => obj.name == "mon1");
        const monster2 = this.map2.findObject("MonsterLayer" , obj => obj.name == "mon2");
        const monster3 = this.map2.findObject("MonsterLayer" , obj => obj.name == "mon3");
        const monster4 = this.map2.findObject("MonsterLayer" , obj => obj.name == "mon4");
        const monster5 = this.map2.findObject("MonsterLayer" , obj => obj.name == "mon5");
        const monster6 = this.map2.findObject("MonsterLayer" , obj => obj.name == "mon6");
        const monster7 = this.map2.findObject("MonsterLayer" , obj => obj.name == "mon7");
        const monster8 = this.map2.findObject("MonsterLayer" , obj => obj.name == "mon8");

        
        this.monsterGroup = this.physics.add.group();
        this.monsterGroup.add(new Eye(this, monster1.x , monster1.y , this.player) );
        this.monsterGroup.add(new Eye(this, monster2.x , monster2.y , this.player) );
        this.monsterGroup.add(new Eye(this, monster3.x , monster3.y , this.player) );
        this.monsterGroup.add(new Eye(this, monster4.x , monster4.y , this.player) );
        this.monsterGroup.add(new Eye(this, monster5.x , monster5.y , this.player) );
        this.monsterGroup.add(new Eye(this, monster6.x , monster6.y , this.player) );
        this.monsterGroup.add(new Eye(this, monster7.x , monster7.y , this.player) );
        this.monsterGroup.add(new Eye(this, monster8.x , monster8.y , this.player) );

        this.physics.add.collider(this.player, this.worldLayer);
        this.physics.add.collider(this.monsterGroup , this.worldLayer);

       // bg = this.sound.add('bg');
        //bg.play({ loop: true });    //วนลูป

        this.physics.add.overlap(this.player, this.monsterGroup, (b1 ,b2) => {
            if (this.deltaNoDamageTime > 1000) {
                this.hpCount--;
                this.hpDisplay = "";
                for (let i = 0; i < this.hpCount; i++ ) {
                    this.hpDisplay += "* ";
                }
                this.hp_HUD.setText("HP : " +this.hpDisplay);
                this.deltaNoDamageTime = 0;
                

                if (this.hpCount == 0) {
                    this.scene.start("GameOver" , { message : "You died , go to hell." } );
                }


            }
        });


        this.hp_HUD = this.add.text(16, 16, "HP : " +this.hpDisplay, {fontSize : '32px' , fill : '#000000#', fontStyle : 'bold' } );
        this.hp_HUD.setScrollFactor(0);


    }

    processInput(cursors){
        var speed_constant = 300;
        var x_speed_vector = 0;
        var y_speed_vector = 0;

   this.player.body.setVelocity(0);     

    if (cursors.up.isDown) { 
        y_speed_vector -= 1; this.player.anims.play('up_anim', true);
    }
    if (cursors.down.isDown) { 
        y_speed_vector += 1;  this.player.anims.play('down_anim', true);
    }
    if (cursors.left.isDown) { 
        x_speed_vector -= 1; this.player.anims.play('left_anim', true);
    }

    if (cursors.right.isDown) {
        x_speed_vector += 1; this.player.anims.play('right_anim', true);
    }


        if (x_speed_vector != 0 || y_speed_vector != 0) {
            this.player.body.setVelocityX( x_speed_vector * speed_constant);
            this.player.body.setVelocityY( y_speed_vector * speed_constant);
            
        };
    }


   update(time, delta) {
        let cursors = this.input.keyboard.createCursorKeys();
        this.processInput(cursors);
        this.deltaNoDamageTime += delta;

    }
}
