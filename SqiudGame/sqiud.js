class Sqiud extends Phaser.Scene {

    constructor () {
        super("Sqiud");
    }

    preload() {
        this.load.spritesheet('player','assets/playern.png',
                                        {frameWidth : 64 , frameHeight : 64, startFrame: 0, endFrame: 35 } );

        this.load.image('terrain_image','assets/terrain_atlas.png');

        this.load.tilemapTiledJSON('maps2','sqiud.json');

        this.load.audio('echo', 'assets/echo.mp3');

    }
  
    create() {

        this.anims.create({
            key : 'down_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 18, end: 26 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'left_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 9, end: 17 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'right_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 27, end: 35 }),
            frameRate : 10, repeat: -1,
        });

        this.anims.create({
            key : 'up_anim',
            frames : this.anims.generateFrameNumbers('player', { start: 0, end: 8 }),
            frameRate : 10, repeat: -1,
        });

        this.hpCount = 100;
        this.hpDisplay = "* * * * * * * * * * * * * * *";

        this.deltaNoDamageTime = 0;
        
        this.maps2 = this.make.tilemap({key : 'maps2'})
        this.tileset = this.maps2.addTilesetImage('Terrain','terrain_image');

        this.floorLayer = this.maps2.createLayer('Floor',this.tileset);
        this.worldLayer = this.maps2.createLayer('World',this.tileset);
        this.worldLayer.setCollisionByProperty ( { colliders :  true});

        this.sound_echo = this.sound.add('echo');
        this.sound_echo.play({loop:true});

        this.worldLayer.setTileLocationCallback(26 , 25 , 3 , 3 ,
            () => {
                    this.scene.start("Glass" , { hpCount : this.hpCount ,
                                                  hpDisplay : this.hpDisplay });

                    this.sound.stopAll();
            }
        )

        const spawnPoint = this.maps2.findObject("ObjectsLayer", obj =>obj.name == "spawnPoint1");
        this.player = this.physics.add.sprite( spawnPoint.x , spawnPoint.y , 'player');

        this.cameras.main.setBounds(0,0,1600,1600);
        this.cameras.main.startFollow(this.player);
        this.cameras.main.roundPixels = true;

        this.physics.add.collider(this.player,this.worldLayer);
        
        this.physics.add.overlap(this.player, this.monsterGroup, (b1 ,b2) => {
            if (this.deltaNoDamageTime > 1000) {
                this.hpCount--;
                this.hpDisplay = "";
                for (let i = 0; i < this.hpCount; i++ ) {
                    this.hpDisplay += "* ";
                }
                this.hp_HUD.setText("HP : " +this.hpDisplay);
                this.deltaNoDamageTime = 0;
                

                if (this.hpCount == 0) {
                    this.scene.start("GameOver" , { message : "You died , go to hell." } );
                }


            }
        });

        this.hp_HUD = this.add.text(16, 16, "HP : " +this.hpDisplay, {fontSize : '32px' , fill : '#000000#' , fontStyle : 'bold'} );
        this.hp_HUD.setScrollFactor(0);

    }

    processInput (cursors){
        var speed_constant = 300;
        var x_speed_vector = 0;
        var y_speed_vector = 0;

        this.player.body.setVelocity(0);

        if (cursors.up.isDown) { 
                y_speed_vector -= 1; this.player.anims.play('up_anim', true);
        }

        if (cursors.down.isDown) { 
                y_speed_vector += 1;  this.player.anims.play('down_anim', true);
        }

        if (cursors.left.isDown) { 
                x_speed_vector -= 1; this.player.anims.play('left_anim', true);
        }

        if (cursors.right.isDown) {
                x_speed_vector += 1; this.player.anims.play('right_anim', true);
        }


        if (x_speed_vector != 0 || y_speed_vector != 0) {
                this.player.body.setVelocityX( x_speed_vector * speed_constant);
                this.player.body.setVelocityY( y_speed_vector * speed_constant);
                    
        };
    }

     update (time,delta) {
            let cursors = this.input.keyboard.createCursorKeys();
            this.processInput(cursors);
            this.deltaNoDamageTime += delta;

     }

}